# MAC-Rules

Sandboxing for Linux with [MAC](https://en.wikipedia.org/wiki/Mandatory_access_control) and [Seccomp BPF](https://www.kernel.org/doc/html/v4.16/userspace-api/seccomp_filter.html).
